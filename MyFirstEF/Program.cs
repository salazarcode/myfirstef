﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Hola mundo!");
            var context = new prubasEntities();
            var post = new Posts()
            {
                id = 2,
                autor = "Eduardo",
                texto = "Otro texto de blog"
            };
            context.Posts.Add(post);
            context.SaveChanges();
            */

            SqlConnection conn = new SqlConnection("Server=.; Database=prubasEntity; Trusted_Connection=True;");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "sp_insertar_post";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@autor", SqlDbType.VarChar).Value = "Sergio Samuel";
            cmd.Parameters.Add("@texto", SqlDbType.VarChar).Value = "Erase una vez una caperusita rosa";
            cmd.Connection = conn;

            conn.Open();

            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Console.WriteLine("Id creado: " + reader["creado"].ToString());
            }

            conn.Close();


        }
    }
}
